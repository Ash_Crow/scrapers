scrapers
========

Python html scrapers.

## Installation
- Clone the repository somewhere
- `make install` will
  - install the virtualenv using poetry
  - install the pre-commit hooks
