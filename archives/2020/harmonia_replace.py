#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import path
import re

# Making it so the script can work in a couple different locations.
if path.isdir("resources/"):
    rootpath = "resources/"
else:
    rootpath = "."

filename_in = "Rynn1-133.html"
filename_out = "Rynn1-133-fixed.html"

filepath_in = path.join(rootpath, filename_in)
filepath_out = path.join(rootpath, filename_out)

with open(filepath_in, "r", encoding="utf-8") as in_file:
    input_text = in_file.read()
    output_text = re.sub(
        r'<\/p><p class="Standard">[\s　‌]+<\/p><p class="Standard">',
        '</p>\n<p class="paragraphe">',
        input_text,
    )
    output_text = output_text.replace('</p><p class="Standard">', " ")
    output_text = output_text.replace("\u3000", " ")
    output_text = output_text.replace("\u200c", "")

    print(output_text)

with open(filepath_out, "w") as out_file:
    out_file.write(output_text)
