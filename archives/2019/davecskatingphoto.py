#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from os import path, rename
from pathlib import Path
from pprint import pprint
import argparse
import re
import validators
import requests
import shutil

WEBROOT = "http://davecskatingphoto.com"

# Making it so the script can work in a couple different locations.
if path.basename(path.dirname(path.realpath(__file__))) == "davecskatingphoto.com":
    rootpath = "."
elif path.isdir("resources/davecskatingphoto.com"):
    rootpath = "resources/davecskatingphoto.com"
else:
    rootpath = "davecskatingphoto.com"


def parse_photos_page(photos_file, comp_name):
    print("Parsing {} page".format(photos_file))
    photos = []
    try:
        if validators.url(photos_file):
            photos_page = requests.get(photos_file)
            photos_soup = BeautifulSoup(photos_page.text, "lxml")
        else:
            photos_page = path.join(rootpath, photos_file)
            photos_soup = BeautifulSoup(
                open(photos_page, encoding="ISO-8859-1"), "lxml"
            )

        thumbs = photos_soup.select("div.photothumbs > a")

        for thumb in thumbs:
            photo_path = thumb["href"]
            photo_parts = photo_path.split("/")
            comp_folder = photo_parts[2]
            if ("senior" in photo_path) or ("junior" in photo_path):
                page_folder = "{} {}".format(photo_parts[3], photo_parts[4])
            else:
                page_folder = photo_parts[3]
            img_name = photo_parts[-1]
            img_name_parts = img_name.split(".")
            img_number = img_name_parts[0].split("_")[-1]
            img_format = img_name_parts[-1]

            if not len(comp_name):
                comp_name = comp_folder

            if page_folder == img_name:
                page_folder = ""

            if "title" in thumb:
                name = thumb["title"]
            else:
                name = thumb.img["alt"]
            name = re.sub(r"Photo ?(of )?(the )?", "", name)

            final_name = "{} {} - {} - {}.{}".format(
                comp_name, page_folder.title(), name, img_number, img_format
            ).replace("/", "-")
            final_name = re.sub("\s\s+", " ", final_name).replace("- -", "-")

            photos.append(
                {
                    "path": photo_path,
                    "competition": comp_name,
                    "page": page_folder,
                    "number": img_number,
                    "format": img_format,
                    "filename": img_name,
                    "final_name": final_name,
                }
            )
    except:
        raise Exception("Could not parse page {}".format(photos_file))

    if len(photos):
        print("  {} photos found for {}".format(len(photos), page_folder))
    else:
        print("  No photo found for this page")
    return photos


def parse_competition_page(file, mode, comp_title=""):
    if validators.url(file):
        url_mode = True
        competition_page = requests.get(file)
        comp_soup = BeautifulSoup(competition_page.text, "lxml")
    else:
        url_mode = False
        competition_page = path.join(rootpath, file)
        comp_soup = BeautifulSoup(open(competition_page, encoding="ISO-8859-1"), "lxml")

    if not len(comp_title):
        comp_title = comp_soup.title.string.split(" - ")[-1].strip()
    print("Competition name: {}".format(comp_title))

    if mode == "interactive":
        user_input = ""
        print("You can change the competition name or press Enter:")
        user_input = input()
        if len(user_input):
            comp_title = user_input

    links = comp_soup.select("a")

    subpages = [file]
    for link in links:
        if "men.html" in link["href"]:
            subpages.append(link["href"])
        elif "ladies.html" in link["href"]:
            subpages.append(link["href"])
        elif "pairs.html" in link["href"]:
            subpages.append(link["href"])
        elif "dance.html" in link["href"]:
            subpages.append(link["href"])
        elif "banquet.html" in link["href"]:
            subpages.append(link["href"])
        elif "gala.html" in link["href"]:
            subpages.append(link["href"])
        elif "opening.html" in link["href"]:
            subpages.append(link["href"])
        elif "podiums.html" in link["href"]:
            subpages.append(link["href"])
        elif "competition.html" in link["href"]:
            subpages.append(link["href"])
    pprint(subpages)

    all_photos = []
    for s in subpages:
        if url_mode and not validators.url(s):
            s = path.join(WEBROOT, s)
        all_photos += parse_photos_page(s, comp_title)

    print("Total: {} photos found.".format(len(all_photos)))

    if mode == "details":
        for p in all_photos:
            print(
                "Original name: {} - New name: {}".format(
                    p["filename"], p["final_name"]
                )
            )
    elif mode == "interactive":
        user_input = ""
        while user_input not in ["y", "n", "q"]:
            print("Show detailed list? (y/n)")
            user_input = input().lower()
        if user_input == "y":
            for p in all_photos:
                print(
                    "Original name: {} - New name: {}".format(
                        p["filename"], p["final_name"]
                    )
                )

        if user_input != "q":
            while user_input not in ["1", "2", "d", "q"]:
                print("Choose next action:")
                if url_mode:
                    print("D. Download the files locally")
                else:
                    print("1. Rename files to the new filename")
                    print("2. Revert to the original filename")
                print("Q. Quit")
                user_input = input().lower()
            if user_input == "d":
                mode = "download"
            if user_input == "1":
                mode = "rename"
            elif user_input == "2":
                mode = "revert"

    rename_count = 0

    if mode == "download":
        output_path = Path(path.join("outputs", comp_title))
        output_path.mkdir(parents=True, exist_ok=True)
        for p in all_photos:
            print(f'Downloading {p["final_name"]}')
            file_url = path.join(WEBROOT, p["path"][2:])
            response = requests.get(file_url, stream=True)
            output_file = path.join(output_path, p["final_name"])
            with open(output_file, "wb") as out_file:
                shutil.copyfileobj(response.raw, out_file)
            del response
    elif mode == "rename":
        for p in all_photos:
            if rename_file(p["path"], p["filename"], p["final_name"]):
                rename_count += 1
    elif mode == "revert":
        for p in all_photos:
            if rename_file(p["path"], p["final_name"], p["filename"]):
                rename_count += 1

    if rename_count > 0:
        print("{} files renamed".format(rename_count))


def rename_file(full_path, filename, final_name):
    file_path = path.dirname(full_path)
    try:
        rename(
            path.join(rootpath, file_path, filename),
            path.join(rootpath, file_path, final_name),
        )
        return True
    except:
        print("could not rename file {}".format(filename))


if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description="Parse ice skating competition photos and rename them."
    )
    argparser.add_argument(
        "filename",
        help='The name of an ice skating competition webpage, e.g "photos_2010_canadians.html"',
    )
    argparser.add_argument(
        "--mode",
        choices=["rename", "interactive", "revert", "details", "download", "count"],
        default="interactive",
        help="Chose the action mode",
    )
    argparser.add_argument(
        "--competition",
        "-c",
        help='The name of an ice skating competition, e.g "2010 Canadian Figure Skating Championships"',  # noqa
    )
    args = argparser.parse_args()

    input_file = args.filename
    print("Running in {} mode.".format(args.mode))
    if input_file:
        print("Parsing photos for {}".format(input_file))
        if args.competition:
            parse_competition_page(input_file, args.mode, args.competition)
        else:
            parse_competition_page(input_file, args.mode)
