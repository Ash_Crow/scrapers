import os
import requests
from bs4 import BeautifulSoup

"""
index_page = "https://www.rennesencheres.com/ventes/ar-seiz-breur-1923-2023-le-centenaire-1042.html"
"""

BASE_URL = "https://www.rennesencheres.com"
OUTPUT_FOLDER = "outputs/seizbreur"


if not os.path.exists(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)


def download_image(url: str, filename: str) -> None:
    file_path = os.path.join(OUTPUT_FOLDER, filename)

    r = requests.get(url, stream=True)
    if r.ok:
        with open(file_path, "wb") as f:
            for chunk in r.iter_content(chunk_size=1024 * 8):
                if chunk:
                    f.write(chunk)
                    f.flush()
                    os.fsync(f.fileno())
    else:  # HTTP status code 4XX/5XX
        print("Download failed: status code {}\n{}".format(r.status_code, r.text))


def scrape_lot(page_url: str) -> None:
    response = requests.get(page_url)
    soup = BeautifulSoup(response.text, features="lxml")

    title = soup.find("h1").text
    print(f"Downloading {title}")

    base_file_name = title.lower().replace(" ", "_")

    description = soup.find(id="description").text.split("Envoyer à un ami")[0].strip()

    desc_file = f"{OUTPUT_FOLDER}/{base_file_name}.txt"
    f = open(desc_file, "w")
    f.write(f"{title} :")
    f.write("\n")
    f.write(description)
    f.write("\n")
    f.write(page_url)
    f.close()

    photo_lot = soup.find(id="photo-lot")
    if photo_lot:
        img_file = photo_lot["data-zoom-image"].strip().split("&")[0]
        img_file_resized = f"{img_file}&w=2000&h=2000"
        download_image(img_file_resized, filename=f"{base_file_name}.jpg")

    # extra pictures
    fancy = soup.find_all(class_="fancybox")
    if fancy:
        for k, v in enumerate(fancy):
            extra_image_file = v.find("img").get("src").strip().split("&")[0]
            extra_image_file_resized = f"{extra_image_file}&w=2000&h=2000"
            extra_image_name = f"{base_file_name}_{k}.jpg"
            download_image(extra_image_file_resized, filename=extra_image_name)

    next_page = soup.find(alt="Lot suivant")

    if next_page:
        next_page_url = f"{BASE_URL}/{next_page.parent.get('href')}"
        scrape_lot(next_page_url)


first_page = f"{BASE_URL}/ventes/ar-seiz-breur-1923-2023-le-centenaire/jorj-robin-1904-1928-sant-erwan-gravure-sur-bois-1-312950.html#lot"  # noqa
scrape_lot(first_page)
