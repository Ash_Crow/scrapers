#!/usr/bin/python3.4
# -*-coding:utf-8 -*
# This code is released under WTFPL Version 2 (http://www.wtfpl.net/)

import csv

insee_data = "resources/France2018.csv"
insee_qid = "resources/query2.csv"

source = 'S248\tQ2981593\tS854\t"https://www.insee.fr/fr/information/3363419"\tS813\t+2019-01-06T00:00:00Z/11'
names = []
with open(insee_data, "r") as insee_data_file:
    reader = csv.DictReader(insee_data_file, delimiter=";", quotechar='"')

    for row in reader:
        insee = row["DEP"] + row["COM"]
        article = row["ARTMIN"]
        name = row["NCCENR"]
        if article:
            article = article[1:-1]
            if article == "L'":
                name = article + name
            else:
                name = article + " " + name
        names.append((insee, name))

print(names)
all_insee = []
items_by_insee = {}
diverging_items = {}
count = 0
with open(insee_qid, "r") as insee_qid_file:
    reader = csv.DictReader(insee_qid_file)
    for row in reader:
        insee = row["insee"]
        qid = row["item"].split("/")[-1]
        name = row["itemLabel"]
        if (insee, name) in names:
            items_by_insee[insee] = {"qid": qid, "name": name}
        else:
            diverging_items[insee] = {"qid": qid, "name": name}

print(len(items_by_insee))
print(len(diverging_items))

correct_results = ""
warnings = ""
for n in names:
    insee = n[0]
    official_name = n[1]
    if insee in items_by_insee:
        result_string = '{}\t{}\tfr:"{}"\t{}'.format(
            items_by_insee[insee]["qid"], "P1448", official_name, source
        )
        correct_results = correct_results + result_string + "\n"
    elif insee in diverging_items:
        result_string = "Insee code {} ({}) not found with the same name, but is on Wikidata as {} ({})".format(
            insee,
            official_name,
            diverging_items[insee]["qid"],
            diverging_items[insee]["name"],
        )
        warnings = warnings + result_string + "\n"
    else:
        result_string = "Insee code {} ({}) not found".format(insee, official_name)
        warnings = warnings + result_string + "\n"

with open("outputs/ok.txt", "w") as correct_results_file:
    correct_results_file.write(correct_results)

with open("outputs/ko.txt", "w") as warnings_file:
    warnings_file.write(warnings)
