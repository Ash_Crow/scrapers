import csv
from functools import partial, partialmethod
import logging


class LogFormatter(logging.Formatter):
    """
    Custom log formatter.

    - Adds a custom "SUCCESS" level set between INFO and WARNING
    - Colours the output
    """

    logging.SUCCESS = 25  # type: ignore
    logging.addLevelName(logging.SUCCESS, "SUCCESS")  # type: ignore
    logging.Logger.success = partialmethod(logging.Logger.log, logging.SUCCESS)  # type: ignore
    logging.success = partial(logging.log, logging.SUCCESS)  # type: ignore

    GREY = "\x1b[38;20m"
    YELLOW = "\x1b[33;20m"
    GREEN = "\x1b[32;20m"
    RED = "\x1b[31;20m"
    BOLD_RED = "\x1b[31;1m"
    RESET = "\x1b[0m"
    LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"

    FORMATS = {
        logging.DEBUG: f"{GREY}{LOG_FORMAT}{RESET}",
        logging.INFO: f"{GREY}{LOG_FORMAT}{RESET}",
        logging.SUCCESS: f"{GREEN}{LOG_FORMAT}{RESET}",  # type:ignore
        logging.WARNING: f"{YELLOW}{LOG_FORMAT}{RESET}",
        logging.ERROR: f"{RED}{LOG_FORMAT}{RESET}",
        logging.CRITICAL: f"{BOLD_RED}{LOG_FORMAT}{RESET}",
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def write_csv(
    rows: list[dict], fieldnames: list[str], filename: str = "output"
) -> None:
    output_path = f"outputs/{filename}.csv"
    logging.info(f"Writing content to file {output_path}")

    with open(output_path, "w") as csv_output:
        writer = csv.DictWriter(
            csv_output, fieldnames=fieldnames, extrasaction="ignore"
        )
        writer.writeheader()

        for row in rows:
            writer.writerow(row)
