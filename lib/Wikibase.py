from SPARQLWrapper import SPARQLWrapper, JSON


class Wikibase:
    WIKIDATA_ENDPOINT = "https://query.wikidata.org/bigdata/namespace/wdq/sparql"
    AGENT = "Ash_Crow scraper tools for Wikibase"

    def sparql_query(self, query, endpoint=WIKIDATA_ENDPOINT, agent=AGENT):
        """
        Queries WDQS and returns the result
        """
        sparql = SPARQLWrapper(endpoint, agent)
        sparql.setQuery(query)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        return results

    def statement(self, prop, value, source):
        """
        Returns a statement in the QuickStatements experted format
        """
        return 'LAST\t{}\t{}\tS854\t"{}"'.format(prop, value, source)
