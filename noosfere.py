from bs4 import BeautifulSoup
import logging
import requests
import string
import timeit
from unidecode import unidecode

from lib.constants import REQUESTS_HEADERS, REQUESTS_TIMEOUT
from lib.utils import LogFormatter, write_csv

PUBLISHER_URL_BASE = "https://www.noosfere.org/livres/editeur.asp?numediteur="

PUBLISHERS_LIST_URL_BASE = "https://www.noosfere.org/livres/editeurs.asp?Lettre="

# Logs
logger = logging.getLogger()
logger.setLevel(logging.INFO)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

ch.setFormatter(LogFormatter())

logger.addHandler(ch)


def clean_name(name: str) -> str:
    """
    Puts some names back in the normal order
    """
    PUT_FIRST = [
        "association",
        "de l'",
        "de la",
        "edition du",
        "editions",
        "editions d'",
        "editions de",
        "editions de l'",
        "editions de la",
        "editions des",
        "editions du",
        "editions la",
        "les editions de la",
        "les editions du",
        "librairie",
        "publications",
        "revue",
    ]
    if " (" in name:
        name_parts = name.replace(")", "").split(" (")

        # Fix a typo in https://www.noosfere.org/livres/editeur.asp?numediteur=2078947405
        if name_parts[1] == "écitions du":
            name_parts[1] = "éditions du"

        if unidecode(name_parts[1]).lower() in PUT_FIRST:
            name = f"{name_parts[1]} {name_parts[0]}"

    name = name[0].upper() + name[1:]
    return name


def scrape_collections_list(editors) -> list:
    """
    Gets the list of current and past collections from Noosfere.

    The Collections page on the site only has current editions, so we need to parse from the editors pages
    """

    all_collections = []
    for editor in editors:
        editor_name = editor["name"]
        editor_id = editor["id"]

        response = requests.get(
            PUBLISHER_URL_BASE + editor_id,
            timeout=REQUESTS_TIMEOUT,
            headers=REQUESTS_HEADERS,
        )

        soup = BeautifulSoup(response.text, features="lxml")
        collections_table = soup.find("table", {"id", "noocadre_height200"}).find("table")  # type: ignore

        # First result is the legend
        raw_collections = collections_table.find_all("tr")[1:]  # type: ignore

        i = 0
        for rc in raw_collections:
            collection = parse_collection_entry(rc)

            if collection["id"] != "0":
                collection["editor_id"] = editor_id
                date = collection.get("date", "")
                collection["description"] = (
                    f"Editorial collection by {editor_name} {date}"
                ).strip()
                collection["P31"] = "Q20655472"  # Instance of: editorial collection
                all_collections.append(collection)
                i += 1

        logging.info(
            f"{i} collection{'s'[:i>1]} parsed for editor {editor_name} ({editor_id})"
        )

    return all_collections


def parse_collection_entry(collection_entry):
    collection = {}
    collection_url = collection_entry.a["href"]
    collection["id"] = collection_url.split("&")[0].split("=")[1]

    ya_coll = collection_entry.find_all("td", "item_jeun")
    if len(ya_coll):
        collection["ya"] = True

    current_coll = collection_entry.find_all("td", "item_coll")
    if len(current_coll):
        collection["current"] = True
    else:
        collection["current"] = False

    collection["name"] = collection_entry.a.text.strip()
    date = collection_entry.a.next_sibling
    if date:
        collection["date"] = date.strip()

    return collection


def scrape_editors_list(page_id: str) -> list:
    """Page id values : letters from A to Z and 'NUM'."""

    COUNTRIES = {
        "belgium": "Q31",
        "cameroon": "Q1009",
        "canada": "Q16",
        "china": "Q148",
        "egypt": "Q79",
        "finland": "Q33",
        "france": "Q142",
        "germany": "Q183",
        "iceland": "Q189",
        "italy": "Q38",
        "lebanon": "Q822",
        "luxembourg": "Q32",
        "moldova": "Q217",
        "monaco": "Q235",
        "netherlands": "Q29999",
        "nigeria": "Q1033",
        "romania": "Q218",
        "russia": "Q159",
        "sweden": "Q34",
        "switzerland": "Q39",
        "tunisia": "Q948",
        "united_kingdom": "Q145",
        "united_states_of_america": "Q145",
    }

    logging.info(f"Parsing page {page_id}")
    response = requests.get(
        PUBLISHERS_LIST_URL_BASE + page_id,
        timeout=REQUESTS_TIMEOUT,
        headers=REQUESTS_HEADERS,
    )
    soup = BeautifulSoup(response.text, features="lxml")
    raw_editors = soup.find_all("td", "item_puce")
    parsed_editors = []

    for e in raw_editors:
        editor = {}
        editor["name"] = clean_name(e.text.strip())
        editor["id"] = e.a["href"].split("=")[1]
        editor["P31"] = "Q2085381"

        # Editor's country is in the flag
        image = e.img
        if image:
            country = (
                image["src"].replace("/images/drapeaux/", "").replace("_a-01.gif", "")
            )
            if country and country not in COUNTRIES:
                logging.warning(f"Country not found: {country}")

            editor["P17"] = COUNTRIES.get(country, "")

        parsed_editors.append(editor)

    return parsed_editors


def scrape_editors(write_to_file: bool = False) -> list:
    all_editors = []
    for letter in string.ascii_uppercase:
        all_editors += scrape_editors_list(letter)
    all_editors += scrape_editors_list("NUM")

    if write_to_file:
        fieldnames = ["id", "name", "P31", "P17"]
        filename = "noosfere_editors"
        write_csv(rows=all_editors, fieldnames=fieldnames, filename=filename)

        logger.success(f"Output file generated with {len(all_editors)} editors listed")  # type: ignore
    else:
        logger.success(f"{len(all_editors)} editors listed")  # type: ignore

    return all_editors


def scrape_collections(editors, write_to_file: bool = False) -> list:
    all_collections = scrape_collections_list(editors)

    if write_to_file:
        fieldnames = [
            "id",
            "name",
            "description",
            "P31",
            "current",
            "date",
            "editor_id",
        ]
        filename = "noosfere_collections"
        write_csv(rows=all_collections, fieldnames=fieldnames, filename=filename)

        logger.success(f"Output file generated with {len(all_collections)} collections listed")  # type: ignore
    else:
        logger.success(f"{len(all_collections)} collections listed")  # type: ignore

    return all_collections


if __name__ == "__main__":
    start = timeit.default_timer()

    editors = scrape_editors()
    collections = scrape_collections(
        editors,
        write_to_file=True,
    )

    stop = timeit.default_timer()
    logger.info(f"Execution time: {stop - start} seconds")
