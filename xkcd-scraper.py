#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
from lib.Wikibase import Wikibase

languages = ["fr", "en", "br", "de"]

ROOT_URL = "http://www.xkcd.com"
INDEX_URL = ROOT_URL + "/archive/index.html"


def ordinal(value):
    """
    Converts zero or a *positive* integer (or its string
    representation) to an ordinal value.
    """
    try:
        value = int(value)
    except ValueError:
        return value

    if value % 100 // 10 != 1:
        if value % 10 == 1:
            ordval = "%d%s" % (value, "st")
        elif value % 10 == 2:
            ordval = "%d%s" % (value, "nd")
        elif value % 10 == 3:
            ordval = "%d%s" % (value, "rd")
        else:
            ordval = "%d%s" % (value, "th")
    else:
        ordval = "%d%s" % (value, "th")

    return ordval


# Get what is already in Wikidata to ignore it
def get_imported_episodes():
    imported_episodes = []

    results = Wikibase.sparql_query(
        """
    SELECT DISTINCT ?episode ?episodeLabel ?number WHERE {
      ?episode wdt:P31 wd:Q838795 .
      ?episode wdt:P361 wd:Q13915 .
      ?episode wdt:P433 ?number .

      SERVICE wikibase:label {
        bd:serviceParam wikibase:language "en" .
      }
    } ORDER BY xsd:integer(?number)
    """
    )
    # """, agent="XKCD scraper for Wikidata")

    for r in results["results"]["bindings"]:
        try:
            imported_episodes.append(int(r["number"]["value"]))
        except:
            print("{} has no episode number.".format(r))
    return imported_episodes


# Get the episodes list
def get_source_episodes():
    response = requests.get(INDEX_URL)
    soup = BeautifulSoup(response.text, "lxml")

    episodes = soup.select("div#middleContainer a")
    episodes.reverse()

    return episodes


def format_episode_data(episode, latest_imported_episode):
    formated_episode = {}
    formated_episode["title"] = '"' + episode.get_text() + '"' or ""
    urlbit = episode.attrs.get("href") or ""
    formated_episode["episode_url"] = ROOT_URL + urlbit
    formated_episode["episodenumber"] = urlbit.replace("/", "")

    formated_episode["date"] = (
        "+"
        + "-".join(["{0:0>2}".format(v) for v in e.attrs.get("title").split("-")])
        + "T00:00:00Z/11"
        or ""
    )
    return formated_episode


if __name__ == "__main__":
    imported_episodes = get_imported_episodes()

    latest_imported_episode = max(imported_episodes)

    for i in range(1, latest_imported_episode):
        # There is no episode 404.
        if i not in imported_episodes and i != 404:
            print("Episode {} is missing :(".format(i))

    episodes = get_source_episodes()

    for e in episodes:
        fe = format_episode_data(e, latest_imported_episode)

        if int(fe["episodenumber"]) > latest_imported_episode:
            print("CREATE")
            for lang in languages:
                print("LAST\tL{}\t{}".format(lang, fe["title"]))
                print('LAST\tA{}\t"xkcd {}"'.format(lang, fe["episodenumber"]))

            print('LAST\tDfr\t"strip de xkcd n°{}"'.format(fe["episodenumber"]))
            print('LAST\tDde\t"Folge des Webcomics xkcd"')
            print(
                'LAST\tDen\t"{} strip of the webcomic xkcd"'.format(
                    ordinal(fe["episodenumber"])
                )
            )

            print(
                Wikibase.statement("P31", "Q838795", fe["episode_url"])
            )  # instance of
            print(
                Wikibase.statement("P1476", "en:" + fe["title"], fe["episode_url"])
            )  # instance of
            print(Wikibase.statement("P361", "Q13915", fe["episode_url"]))  # part of
            print(
                Wikibase.statement(
                    "P433", '"' + fe["episodenumber"] + '"', fe["episode_url"]
                )
            )  # nb
            print(Wikibase.statement("P577", fe["date"], fe["episode_url"]))  # date
            print(
                Wikibase.statement("P50", "Q285048", fe["episode_url"])
            )  # Author: R. Munroe
            print(
                Wikibase.statement(
                    "P2699", '"' + fe["episode_url"] + '"', fe["episode_url"]
                )
            )  # URL
            print(
                Wikibase.statement("P407", "Q1860", fe["episode_url"])
            )  # Language: English
            print(
                Wikibase.statement("P275", "Q6936496", fe["episode_url"])
            )  # Licence: CC-BY-NC
            print("")
